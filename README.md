## Deploy

### Tests
Инфраструктура для тестирования.

`docker-compose -f ./Build/test.yml up --build`

### Main
Основная инфраструктура. Пример работы сервиса.

`docker-compose -f ./Build/main.yml up --build`

## API сервиса
Ниже написаны примеры вызовов методов сервиса.

#### Основное
1. http://localhost/api/GetEmployeesWithSameSalary
   > возвращает работников, получающих одинаковую зарплату
2. http://localhost/api/GetEmployeesWithMaxSalary
   > возвращает работников, получающих наибольшую зарплату в своем департаменте
3. http://localhost/api/GetEmployeesWithMaxBonus?year=2019
   > возвращает работников, получивших больше бонусов за год в своем департаменте
4. http://localhost/api/GetLeadsWithTeams
   > возвращает всех лидов и членов их команды (формат показан ниже на примере)
5. http://localhost/api/AddBonus?leadId=1&bonus=499&year=2019&month=08&day=08
   > начисляет бонус всем сотрудникам заданного руководителя, кому еще не был начислен бонус в заданном году
6. http://localhost/api/AddBonusRecursive?leadId=1&bonus=499&year=2019&month=08&day=08
   > начисляет бонус всем (**рекурсивно**) сотрудникам заданного руководителя, кому еще не был начислен бонус в заданном году

#### Дополнительно

7. http://localhost/api/GetWorkerTable
   > возвращает содержимое таблицы Worker
8. http://localhost/api/GetBonusTable
   > возвращает содержимое таблицы Bonus
