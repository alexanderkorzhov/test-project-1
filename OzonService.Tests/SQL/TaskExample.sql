﻿insert into Worker (LEAD_ID, FIRST_NAME, LAST_NAME, SALARY, JOINING_DATE, DEPARTMENT) values
    (NULL, 'Mikhail', 'Ivanov',   100000, '2014-02-20', 'HR'),
    (NULL, 'Ivan',    'Zhukov',    80000, '2014-06-11', 'Marketing'),
    (1,    'Zoya',    'Sinicina',  40000, '2014-02-20', 'HR'),
    (NULL, 'Anne',    'Smirnova',  50000, '2014-02-20', 'IT'),
    (3,    'Petr',    'Petrov',     3000, '2014-02-20', 'HR');

insert into Bonus (WORKER_ID, BONUS_DATE, BONUS_AMOUNT) values
    (1, '2019-02-20', 5000),
    (2, '2019-06-11', 3000),
    (2, '2019-02-20', 4000),
    (4, '2019-02-20', 4500),
    (2, '2019-06-11', 3500);