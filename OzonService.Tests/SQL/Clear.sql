﻿DROP TABLE IF EXISTS Worker, Bonus CASCADE;

create table Worker
(
    ID              serial      PRIMARY KEY,
    LEAD_ID         integer     NULL references Worker(ID),
    FIRST_NAME      text        NOT NULL,
    LAST_NAME       text        NOT NULL,
    SALARY          integer     NOT NULL CHECK(SALARY > 0),
    JOINING_DATE    date        NOT NULL,
    DEPARTMENT      text        NOT NULL
);

create table Bonus
(
    WORKER_ID       integer     NOT NULL references Worker(ID),
    BONUS_DATE      date        NOT NULL,
    BONUS_AMOUNT    integer     NOT NULL CHECK(BONUS_AMOUNT > 0)
);