using System;
using System.Linq;
using NUnit.Framework;

namespace OzonService.Tests.Tests
{
    /// <summary>
    /// ���� ����������� �������, ������� ������� � ������� �������.
    /// </summary>
    public sealed class TaskExample : TestContext
    {
        [Test]
        public void GetEmployeesWithMaxSalary()
        {
            var result = Controller.GetEmployeesWithMaxSalary().Result;
            AssertJson(ReadJson(), result.Value);
        }

        [Test]
        [TestCase(2000)]
        [TestCase(2019)]
        public void GetEmployeesWithMaxBonus(int year)
        {
            var result = Controller.GetEmployeesWithMaxBonus(year).Result;
            AssertJson(ReadJson(year.ToString()), result.Value);
        }

        [Test]
        public void GetEmployeesWithSameSalary()
        {
            var result = Controller.GetEmployeesWithSameSalary().Result;
            AssertJson(ReadJson(), result.Value);
        }

        [Test]
        public void GetLeadsWithTeams()
        {
            var result = Controller.GetLeadsWithTeams().Result;
            AssertJson(ReadJson(), result.Value);
        }

        [Test]
        [TestCase(1, 499, 2019, 08, 08)]
        [TestCase(2, 499, 2019, 08, 08)]
        [TestCase(3, 499, 2019, 08, 08)]
        public void AddBonus(
            int leadId,
            int bonus,
            int year,
            int month,
            int day)
        {
            Controller.AddBonus(leadId, bonus, year, month, day).Wait();
            var result = Controller.GetBonusTable().Result;
            AssertJson(ReadJson(CombineArgs(leadId, bonus, year, month, day)), result.Value);
        }

        [Test]
        [TestCase(1, 499, 2019, 08, 08)]
        [TestCase(2, 499, 2019, 08, 08)]
        [TestCase(3, 499, 2019, 08, 08)]
        public void AddBonusRecursive(
            int leadId,
            int bonus,
            int year,
            int month,
            int day)
        {
            Controller.AddBonusRecursive(leadId, bonus, year, month, day).Wait();
            var result = Controller.GetBonusTable().Result;
            AssertJson(ReadJson(CombineArgs(leadId, bonus, year, month, day)), result.Value);
        }

    }
}