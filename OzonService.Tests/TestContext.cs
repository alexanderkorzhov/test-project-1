using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using NUnit.Framework;
using OzonService.Controllers;

namespace OzonService.Tests
{
    /// <summary>
    /// ������� ����� ��� ����-������.
    /// ����� �� ������������ BDD https://en.wikipedia.org/wiki/Behavior-driven_development � ���������� ������. 
    /// </summary>
    public abstract class TestContext
    {
        #region Private Static Fields

        private static readonly Config DefaultConfig = Config.Create(
            "localhost",
            "db_test_ozon_korzhov",
            5430);

        private static readonly string ClearQuery = File.ReadAllText(
            "./SQL/Clear.sql");
        
        #endregion

        #region Private Fields

        private readonly string _className;
        private readonly Config _config;

        #endregion
        
        #region Constructors

        protected TestContext(Config config = null)
        {
            _className = this.GetType().Name;
            _config = config ?? DefaultConfig;
        }
        
        #endregion
        
        #region Protected

        /// <summary>
        /// ���������� ��������� ��� ����� Json-��������, � ������� ����� ������������� ��������� ����������.
        /// ���������� �������� ����������� ������, �������� ������ � ������� � ��������������� ��������� �����. 
        /// </summary>
        /// <param name="arguments"></param>
        /// <param name="testName"></param>
        /// <returns></returns>
        protected string ReadJson(
            string arguments = null,
            [CallerMemberName] string testName = null)
        {
            return File.ReadAllText(
                "./ExpectedResults/" + 
                _className + "/" + 
                testName + (arguments ?? string.Empty) +  
                ".json");
        }

        protected string CombineArgs(params object[] parameters)
        {
            if (parameters == null ||
                parameters.Length == 0)
                return string.Empty;
            var result = parameters.Aggregate(
                string.Empty,
                (s, o) => s + "_" + o);
            return result;
        }

        /// <summary>
        /// ��������� ������ ������� woker, bonus �� ������ ������, ���������� � SQL-������.
        /// </summary>
        /// <param name="testName"></param>
        protected void Insert(string testName)
        {
            var query = File.ReadAllText("./SQL/" + testName + ".sql");
            using (var conn = new NpgsqlConnection(_config.ConnectionString))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        protected OzonDatabaseController Controller { get; private set; }

        protected static void AssertJson(string expected, string actual)
        {
            var areJsonEquals = AreJsonEquals(expected, actual);
            
            var message = 
                "Expected: " +
                Environment.NewLine +
                expected + 
                Environment.NewLine + 
                "But was:  " +
                Environment.NewLine +
                actual;

            Assert.IsTrue(areJsonEquals, message);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// ����� ������� ������� worker, bonus � ������� �� ������ (��� ���������� worker.id).
        /// ���������� ����� ������ ������.
        /// </summary>
        private void ClearTables()
        {
            using (var conn = new NpgsqlConnection(_config.ConnectionString))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(ClearQuery, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// ��������� ��� ��������������� Json-������� �� ���������������.
        /// </summary>
        /// <param name="json1"></param>
        /// <param name="json2"></param>
        /// <returns></returns>
        private static bool AreJsonEquals(
            string json1,
            string json2)
        {
            var sourceJObject = JsonConvert.DeserializeObject<JContainer>(json1);
            var targetJObject = JsonConvert.DeserializeObject<JContainer>(json2);

            return JToken.DeepEquals(sourceJObject, targetJObject);
        }

        #endregion

        #region TestClass

        [SetUp]
        public void Setup()
        {
            var queries = new SqlQueries();
            var mockLogger = new Mock<ILogger<OzonDatabaseController>>().Object;
            Controller = new OzonDatabaseController(mockLogger, _config, queries);
            ClearTables();
            Insert(_className);
        }

        #endregion
    }
}