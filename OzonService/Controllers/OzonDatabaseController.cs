﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace OzonService.Controllers
{
    [Route("api")]
    [ApiController]
    public sealed class OzonDatabaseController : ControllerBase
    {
        #region Constants / Static Fields
    
        private const string EmptyJsonArray = "[]";

        #endregion

        #region Private Fields

        private readonly Config _config;
        private readonly SqlQueries _queries;

        #endregion

        #region Constructors

        public OzonDatabaseController(
            ILogger<OzonDatabaseController> logger,
            Config config,
            SqlQueries queries)
        {
            _config = config;
            _queries = queries;

            // Зачем здесь эта строка? 
            // Эта строка -- демонстрация успешного inject'a экземпляра логгера.
            // Этот объект де факто не используется, потому что текущая конфигурация логера 
            // пишет в логи всю необходимую информацию, например:
            // 1. URL вызова, из которого можно понять что именно за метод был вызван.
            // 2. Аргументы HTTP-запроса (аргументы методов).
            // 3. Исключения, возникающие в процессе работы приложения, в т.ч. стек вызовов.
            // Поэтому я не вижу явной необходимости логировать что-то дополнительно,
            // но прокинул объект логера в контроллер для того, чтобы можно было что-то
            // добавить в лог, в случае необходимости.

            // Также, можно было бы прокинуть путь к лог-файлам в Docker/Docker-compose,
            // но этого делать не стал из-за того, что пришлось бы указывать конкретную
            // конфигурацию путей для хранимых файлов. Это вопрос удобства и конфигурации
            // развертывания. Отдельно стоит учесть разницу в этом вопросе между
            // unix/windows-системами. Разработка проекта ведется на Win-системе,
            // и мне неизвестно на какой системе проект будут проверять.
            logger.LogInformation("OzonDatabaseController constructor call");
        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Метод выполняет sql-запрос, позволяет конфигурировать команду sql-запроса (например, добавить параметры к запросу)
        /// и вызывает <paramref name="callback"/> для каждой строки результирующей таблицы.
        /// </summary>
        /// <param name="query">Текст sql-запроса</param>
        /// <param name="callback">Callback для обработки строк результирующей таблицы</param>
        /// <param name="configureCommand">Функция, позволяющая дополнительно сконфигурировать команду запроса.</param>
        /// <returns>Возвращает задачу, завершающуюся в момент, когда запрос будет полностью выполнен и обработан.</returns>
        private Task ProcessQuery(
            string query,
            Action<NpgsqlDataReader> callback,
            Action<NpgsqlCommand> configureCommand = null)
        {
            return ProcessQueryStatic(
                _config.ConnectionString,
                query,
                callback,
                configureCommand);
        }

        /// <summary>
        /// Метод следует использовать если SQL-запрос возвращает данные в одной ячейке (1 строка x 1 столбец).
        /// Например, в случае, если запрос возвращает все необходимые данные в виде Json-документа из самой СУБД.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="configureCommand"></param>
        /// <returns></returns>
        private async Task<string> ProcessQueryOneReturnString(
            string query,
            Action<NpgsqlCommand> configureCommand = null)
        {
            string result = null;
            await ProcessQuery(
                query,
                delegate(NpgsqlDataReader reader)
                {
                    if (reader.IsDBNull(0) == false)
                    {
                        // TODO: потоковое чтение работает отлично
                        // TODO: надо найти функцонал записи данных в HTTP Response
                        // TODO: https://gitlab.com/alexanderkorzhov/ozon-test-project/issues/5
                        // result = reader.GetTextReader(0).ReadToEnd();
                        result = reader.GetString(0);
                    }
                },
                configureCommand);
            return result;
        }

        #endregion

        #region Private Static Methods

        private static async Task ProcessQueryStatic(
            string connectionString,
            string query, 
            Action<NpgsqlDataReader> callback,
            Action<NpgsqlCommand> configureCommand = null)
        {
            using (var conn = new NpgsqlConnection(connectionString))
            {
                await conn.OpenAsync();

                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    configureCommand?.Invoke(cmd);
                    await cmd.PrepareAsync();
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            callback(reader);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Поиск в глубину по дереву сотрудников и формирование нужного нам Json-объекта.
        /// </summary>
        /// <param name="relations"></param>
        /// <param name="current"></param>
        /// <param name="isRoot"></param>
        /// <param name="names"></param>
        /// <returns></returns>
        private static JToken DeepFirstSearch(
            IReadOnlyDictionary<int, List<int>> relations,
            int current,
            bool isRoot,
            IReadOnlyDictionary<int, string> names)
        {
            JArray childrenArray = null;

            if (relations.TryGetValue(current, out var children))
            {
                childrenArray = new JArray();
                foreach (var child in children)
                {
                    var jsonChild = DeepFirstSearch(relations, child, false, names);
                    if (jsonChild != null)
                        childrenArray.Add(jsonChild);
                }

            }
            if (isRoot == false)
            {
                var jObject = new JObject();
                jObject.Add("name", names[current]);
                if (childrenArray != null)
                    jObject.Add("team", childrenArray);
                return jObject;
            }
            else
            {
                return childrenArray;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Возвращает работников, получающих одинаковую зарплату, сгруппированных по зарплате.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEmployeesWithSameSalary")]
        public async Task<ActionResult<string>> GetEmployeesWithSameSalary()
        {
            var result = await ProcessQueryOneReturnString(_queries.QueryGetEmployeesWithSameSalary);
            return result ?? EmptyJsonArray; 
        }

        /// <summary>
        /// Возвращает работников, получающих наибольшую зарплату в своем департаменте.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEmployeesWithMaxSalary")]
        public async Task<ActionResult<string>> GetEmployeesWithMaxSalary()
        {
            var result = await ProcessQueryOneReturnString(_queries.QueryGetEmployeesWithMaxSalary);
            return result ?? EmptyJsonArray;
        }
        
        /// <summary>
        /// Возвращает работников, получивших больше бонусов за год в своем департаменте
        /// </summary>
        /// <returns></returns>
        [HttpGet("{year}")]
        [Route("GetEmployeesWithMaxBonus")]
        public async Task<ActionResult<string>> GetEmployeesWithMaxBonus(
            [FromQuery] int year)
        {
            var result = await ProcessQueryOneReturnString(
                _queries.QueryGetEmployeesWithMaxBonus,
                delegate (NpgsqlCommand command)
                {
                    command.Parameters.AddWithValue("start", new DateTime(year, 1, 1));
                    command.Parameters.AddWithValue("end", new DateTime(year + 1, 1, 1));
                });

            return result ?? EmptyJsonArray;
        }

        /// <summary>
        /// Возвращает всех лидов и членов их команды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLeadsWithTeams")]
        public async Task<ActionResult<string>> GetLeadsWithTeams()
        {
            // TODO: Метод крайне низкопроизводительный, для реального использования его необходимо оптимизировать.

            // TODO: SQL-запрос стоит переписать так, чтобы он сразу выдавал конечный Json-документ.
            // TODO: Тогда поиск в глубину и работа с таблицой из-под C# не понадобится.
            // TODO: Тогда же можно будет (видимо, перейдя на ASP .NET Core 3) писать результат 
            // TODO: SQL-запроса в HTTP Response используя Stream и не храня данные в оперативной памяти 
            // TODO: C#-сервиса.
            
            // получаем таблицу Worker (1ая копия).
            var workerTableResult = await GetWorkerTable();
            var workerTableJson = JToken.Parse(workerTableResult.Value);

            // id сотрудника -> список id прямых (без транзитивности) подчиненных.
            // 2ая копия таблицы Worker.
            var relations = new Dictionary<int, List<int>>();
            Action<int, int> addRelation =
                delegate(int id, int lead)
                {
                    var success = relations.TryGetValue(lead, out var list);
                    if (!success)
                    {
                        list = new List<int>();
                        relations.Add(lead, list);
                    }
                    list.Add(id);
                };
            
            // обозначение корня дерева. исходя из структуры таблицы, мы знаем, что
            // реальные ID работников не могут быть равны -1.
            const int rootId = -1;

            // id -> first_name
            var names = new Dictionary<int, string>();
            foreach (var worker in workerTableJson)
            {
                var id = worker["id"].Value<int>();
                var lead = worker["lead_id"].Value<int?>();
                var firstName = worker["first_name"].Value<string>();

                addRelation(id, lead ?? rootId);
                names.Add(id, firstName);
            }

            // 3ая копия таблицы Worker. Получаем нужный Json-объект.
            var result = DeepFirstSearch(relations, rootId, true, names);
            
            return result == null 
                ? EmptyJsonArray 
                // 4ая копия таблицы Worker. Получаем Json в виде строки.
                : JsonConvert.SerializeObject(result);
        }
        
        /// <summary>
        /// Начисляет бонус всем сотрудникам заданного руководителя, кому еще не был начислен бонус в заданном году.
        /// TODO: метод стоит сделать HttpPut или HttpPost для указания факта изменения данных в базе,
        /// TODO: но из-за небольшого размера проекта и удобства ручного тестирования HttpGet, в изменении мало смысла.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AddBonus")]
        public async Task AddBonus(
            [FromQuery] int leadId,
            [FromQuery] int bonus,
            [FromQuery] int year,
            [FromQuery] int month,
            [FromQuery] int day)
        {
            await ProcessQueryOneReturnString(
                _queries.QueryAddBonus,
                delegate (NpgsqlCommand command)
                {
                    command.Parameters.AddWithValue("lead", leadId);
                    command.Parameters.AddWithValue("date", new DateTime(year, month, day));
                    command.Parameters.AddWithValue("year", year);
                    command.Parameters.AddWithValue("bonus", bonus);
                });
        }

        /// <summary>
        /// Начисляет бонус всем (рекурсивно) сотрудникам заданного руководителя, кому еще не был начислен бонус в заданном году.
        /// TODO: метод стоит сделать HttpPut или HttpPost для указания факта изменения данных в базе,
        /// TODO: но из-за небольшого размера проекта и удобства ручного тестирования HttpGet, в изменении мало смысла.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AddBonusRecursive")]
        public async Task AddBonusRecursive(
            [FromQuery] int leadId,
            [FromQuery] int bonus,
            [FromQuery] int year,
            [FromQuery] int month,
            [FromQuery] int day)
        {
            await ProcessQueryOneReturnString(
                _queries.QueryAddBonusRecursive,
                delegate (NpgsqlCommand command)
                {
                    command.Parameters.AddWithValue("lead", leadId);
                    command.Parameters.AddWithValue("date", new DateTime(year, month, day));
                    command.Parameters.AddWithValue("year", year);
                    command.Parameters.AddWithValue("bonus", bonus);
                });
        }

        /// <summary>
        /// Возвращает всё содержимое таблицы Bonus.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetBonusTable")]
        public async Task<ActionResult<string>> GetBonusTable()
        {
            var result = await ProcessQueryOneReturnString(_queries.QueryGetBonusTable);
            return result ?? EmptyJsonArray;
        }

        /// <summary>
        /// Возвращает всё содержимое таблицы Worker.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetWorkerTable")]
        public async Task<ActionResult<string>> GetWorkerTable()
        {
            var result = await ProcessQueryOneReturnString(_queries.QueryGetWorkerTable);
            return result ?? EmptyJsonArray;
        }

        #endregion
    }
}
