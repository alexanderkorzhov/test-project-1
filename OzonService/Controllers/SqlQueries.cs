﻿using System.IO;

namespace OzonService.Controllers
{
    /// <summary>
    /// Контейнер текстов SQL-запросов.
    /// Потенциальное использование может быть таким:
    /// пишем новую оптимизированную версию sql-запроса,
    /// и читаем запрос из другого файла.
    /// </summary>
    public sealed class SqlQueries
    {
        public string QueryGetEmployeesWithSameSalary { get; }
        public string QueryGetEmployeesWithMaxSalary { get; }
        public string QueryGetEmployeesWithMaxBonus { get; }
        public string QueryAddBonus { get; }
        public string QueryGetBonusTable { get; }
        public string QueryGetWorkerTable { get; }
        public string QueryAddBonusRecursive { get; }

        public SqlQueries()
        {
            QueryGetEmployeesWithSameSalary = ReadQuery("GetEmployeesWithSameSalary");
            QueryGetEmployeesWithMaxSalary = ReadQuery("GetEmployeesWithMaxSalary");
            QueryGetEmployeesWithMaxBonus = ReadQuery("GetEmployeesWithMaxBonus");
            QueryAddBonus = ReadQuery("AddBonus");
            QueryAddBonusRecursive = ReadQuery("AddBonusRecursive");
            QueryGetBonusTable = ReadQuery("GetBonusTable");
            QueryGetWorkerTable = ReadQuery("GetWorkerTable");
        }

        // Тут можно было бы указать на https://en.wikipedia.org/wiki/Single_responsibility_principle ,
        // но проект настолько маленький, что в данном случае я считаю, что слепо соблюдать SOLID не стоит.
        private static string ReadQuery(string queryName)
        {
            return File.ReadAllText("./Queries/" + queryName + ".sql");
        }
    }
}