﻿select
   array_to_json(array_agg(result), TRUE) 
from
   (
      select
         worker.id,
         worker.first_name,
         worker.last_name,
         worker.salary,
         worker.department 
      from
         (
            worker 
            join
               (
                  select
                     department,
                     Max(salary) as maxSalary 
                  from
                     worker 
                  group by
                     department 
               )
               as departments 
               on worker.department = departments.department 
               and worker.salary = departments.maxSalary 
         )
   )
   as result;