﻿with recursive recursive_worker as (
    select 
        id,
        lead_id,
        first_name,
        last_name,
        salary,
        joining_date,
        department
    from worker
    where lead_id = @lead
    
    union

    select 
       worker.id,
       worker.lead_id,
       worker.first_name,
       worker.last_name,
       worker.salary,
       worker.joining_date,
       worker.department
    from worker join recursive_worker
        on worker.lead_id = recursive_worker.id
)
insert into Bonus 
    (worker_id, bonus_date, bonus_amount)
    select 
        id,
        @date,
        @bonus
    from (
            recursive_worker
        left join
            (
            select t.id as has_bonus_year
            from (
                select 
                    worker_id as id,
                    cast(date_part('year', bonus_date) as int) as bonus_date
                from bonus
            ) as t
            where t.bonus_date = @year
            group by has_bonus_year) as exclude
        on recursive_worker.id = exclude.has_bonus_year
    )
    where 
        has_bonus_year is null and
        id != @lead;