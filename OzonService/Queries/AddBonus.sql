﻿insert into Bonus 
    (worker_id, bonus_date, bonus_amount)
    select 
        id,
        @date,
        @bonus
    from (
            worker
        left join
            (
            select t.id as has_bonus_year
            from (
                select 
                    worker_id as id,
                    cast(date_part('year', bonus_date) as int) as bonus_date
                from bonus
            ) as t
            where t.bonus_date = @year
            group by has_bonus_year) as exclude
        on worker.id = exclude.has_bonus_year
    )
    where 
        has_bonus_year is null and
        lead_id = @lead;