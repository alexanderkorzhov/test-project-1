﻿with bonuses as 
(
   select
      worker_id as id,
      sum(bonus_amount) as total_bonus 
   from
      bonus 
   where
      bonus_date >= @start
      and bonus_date < @end 
   group by
      worker_id 
),
all_workers as 
(
   select
      worker.id,
      worker.first_name,
      worker.last_name,
      worker.department,
      bonuses.total_bonus 
   from
      worker 
      join
         bonuses 
         on worker.id = bonuses.id 
),
departments as 
(
   select
      department,
      max(total_bonus) as max_bonus 
   from
      worker 
      join
         bonuses 
         on worker.id = bonuses.id 
   group by
      department 
)
select
   array_to_json(array_agg(result), TRUE) 
from
   (
      select
         all_workers.* 
      from
         all_workers 
         join
            departments 
            on all_workers.department = departments.department 
            and all_workers.total_bonus = departments.max_bonus 
   )
   as result;