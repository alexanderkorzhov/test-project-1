﻿select array_to_json(array_agg(groups), TRUE) 
from (
    select salary, array_agg(id) as workers
    from worker
    group by salary
    order by salary asc
) as groups;