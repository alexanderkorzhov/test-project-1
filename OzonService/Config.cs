﻿using Npgsql;

namespace OzonService
{
    public sealed class Config
    {
        public Config(
            string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; }

        // Тут можно было бы указать на https://en.wikipedia.org/wiki/Single_responsibility_principle ,
        // но проект настолько маленький, что в данном случае я считаю, что слепо соблюдать SOLID не стоит.
        public static Config Create(
            string host,
            string databaseName,
            int port)
        {
            var connectionStringBuilder = new NpgsqlConnectionStringBuilder()
            {
                Host = host,
                Username = "postgres",
                Password = "postgres",
                Database = databaseName,
                Port = port
            };
            var connecitonString = connectionStringBuilder.ToString();
            return new Config(connecitonString);
        }
    }
}