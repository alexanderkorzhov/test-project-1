create database db_main_ozon_korzhov;
\c db_main_ozon_korzhov;

create table worker
(
    ID              serial      PRIMARY KEY,
    LEAD_ID         integer     NULL references worker(ID),
    FIRST_NAME      text        NOT NULL,
    LAST_NAME       text        NOT NULL,
    SALARY          integer     NOT NULL CHECK(SALARY > 0),
    JOINING_DATE    date        NOT NULL,
    DEPARTMENT      text        NOT NULL
);

create table bonus
(
    WORKER_ID       integer     NOT NULL references worker(ID),
    BONUS_DATE      date        NOT NULL,
    BONUS_AMOUNT    integer     NOT NULL CHECK(BONUS_AMOUNT > 0)
);