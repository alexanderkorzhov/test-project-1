#!/bin/bash
set -e
database_name="db_main_ozon_korzhov"

sqlQuery(){
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" -c "$1"
}

sqlQuery "CREATE DATABASE $database_name;"