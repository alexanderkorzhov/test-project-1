﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace GenerateInitDatabaseQuery
{
    public static class RandomExtensions
    {
        public static DateTime NextDate(
            this Random random,
            DateTime min,
            DateTime max)
        {
            var daysDelta = (max - min).TotalDays;
            return min.AddDays(random.Next((int) daysDelta));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var databaseName = args[0];

            // т.к. запрос будет выполняться из-под докера на *nix системе,
            // используем явно \n вместо Environment.NewLine и т.п.
            string newLine = Environment.NewLine; //"\n";
            string headerWorker =
                "insert into Worker (LEAD_ID, FIRST_NAME, LAST_NAME, SALARY, JOINING_DATE, DEPARTMENT) values " +
                newLine;
            string headerBonus =
                "insert into Bonus (WORKER_ID, BONUS_DATE, BONUS_AMOUNT) values " +
                newLine;
            var minTime = new DateTime(2000, 1, 1);
            var maxTime = new DateTime(2020, 1, 1);
            const int workersCount = 5000;
            const int bonusCount = 3000;
            const string pathNames = "names/names.txt";
            const string pathSurnames = "names/surnames.txt";
            const string pathOutputInsertWorker = "insertWorker.sql";
            const string pathOutputInsertBonus = "insertBonus.sql";
            const string dateStringFormat = "yyyy-MM-dd";
            var departments = new[] { "HR", "Marketing", "IT" };


            var firstNames = File
                .ReadLines(pathNames)
                .ToArray();
            var lastNames = File
                .ReadLines(pathSurnames)
                .ToArray();

            var random = new Random();

            var workersByDeps = departments.ToDictionary(
                department => department, 
                department => new List<int>());
            var joiningDates = new Dictionary<int, DateTime>();

            using (var writer = new StreamWriter(pathOutputInsertWorker))
            {
                writer.WriteLine("\\c " + databaseName + ";");
                writer.Write(headerWorker);
                for (var i = 0; i < workersCount; ++i)
                {
                    var id = i + 1;
                    if (i > 0)
                    {
                        writer.Write(",");
                        writer.Write(newLine);
                    }

                    int? lead;
                    string department;
                    if (i < departments.Length)
                    {
                        lead = null;
                        department = departments[i];
                    }
                    else
                    {
                        department = departments[random.Next(departments.Length)];
                        var departmentWorkers = workersByDeps[department];
                        lead = departmentWorkers[random.Next(departmentWorkers.Count)];
                    }


                    var leadStr = lead == null 
                        ? "NULL" 
                        : lead.Value.ToString();
                    var firstName = firstNames[random.Next(firstNames.Length)];
                    var lastName = lastNames[random.Next(lastNames.Length)];
                    var salary = (1 + random.Next(100)) * 1000;
                    var joiningDate = random.NextDate(minTime, maxTime);
                    var joiningDateStr = joiningDate.ToString(dateStringFormat, CultureInfo.InvariantCulture);

                    var dataString =
                        $"    ({leadStr}, '{firstName}', '{lastName}', {salary}, '{joiningDateStr}', '{department}')";
                    writer.Write(dataString);

                    workersByDeps[department].Add(id);
                    joiningDates.Add(id, joiningDate);
                }

                writer.Write(";");
            }


            using (var writer = new StreamWriter(pathOutputInsertBonus))
            {
                writer.WriteLine("\\c " + databaseName + ";");
                writer.Write(headerBonus);
                for (var i = 0; i < bonusCount; ++i)
                {
                    var id = 1 + random.Next(workersCount);
                    var bonusAmount = (1 + random.Next(20)) * 1000;
                    var bonusDate = random.NextDate(joiningDates[id], maxTime);
                    var bonusDateStr = bonusDate.ToString(dateStringFormat, CultureInfo.InvariantCulture);

                    if (i > 0)
                    {
                        writer.Write(",");
                        writer.Write(newLine);
                    }
                    
                    var dataString =
                        $"    ({id}, '{bonusDateStr}', {bonusAmount})";
                    writer.Write(dataString);
                }

                writer.Write(";");
            }
            
        }
    }
}